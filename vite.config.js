// noinspection JSUnusedGlobalSymbols

import {defineConfig} from 'vite';
import react from '@vitejs/plugin-react';

export default defineConfig({
  plugins: [react()],
  esbuild: {
    loader: 'jsx', // Ensure esbuild uses the jsx loader for .js files
    include: /\.(js|jsx)$/, // Apply loader to both .js and .jsx files
  },
});
