import {useSelector, useDispatch} from 'react-redux';
import Board from '../../components/Board.jsx';
import {makeMove} from './gameSlice';

const GameBoard = () => {
    const dispatch = useDispatch();
    const board = useSelector((state) => state.game.board);
    const winningLine = useSelector((state) => state.game.winningLine);

    const handleClick = (index) => {
        dispatch(makeMove({index}));
    };

    return <Board board={board} onClick={handleClick} winningLine={winningLine}/>;
};

export default GameBoard;
