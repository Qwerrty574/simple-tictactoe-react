import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    board: Array(9).fill(null),
    xIsNext: true,
    xWins: 0,
    oWins: 0,
    winningLine: null,
};

const gameSlice = createSlice({
    name: 'game',
    initialState,
    reducers: {
        makeMove: (state, action) => {
            const {index} = action.payload;
            if (state.board[index] || calculateWinner(state.board).winner) {
                return;
            }
            state.board = state.board.map((square, idx) =>
                idx === index ? (state.xIsNext ? 'X' : 'O') : square
            );
            state.xIsNext = !state.xIsNext;
        },
        resetBoard: (state) => {
            state.board = Array(9).fill(null);
            state.xIsNext = true;
            state.winningLine = null;
        },
        updateScore: (state, action) => {
            const {winner} = action.payload;
            if (winner === 'X') {
                state.xWins += 1;
            } else if (winner === 'O') {
                state.oWins += 1;
            }
        },
        setWinningLine: (state, action) => {
            state.winningLine = action.payload;
            console.log(state.winningLine)
        },
        resetGame: (state) => {
            state.board = Array(9).fill(null);
            state.xIsNext = true;
            state.xWins = 0;
            state.oWins = 0;
            state.winningLine = null;
        },
    },
});

export const {makeMove, resetBoard, updateScore, setWinningLine, resetGame} = gameSlice.actions;

export default gameSlice.reducer;

export const calculateWinner = (squares) => {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return { winner: squares[a], line: lines[i] };
        }
    }
    return { winner: null, line: null };
};