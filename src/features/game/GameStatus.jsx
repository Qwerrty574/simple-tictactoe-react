import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Box, Typography, Button } from '@mui/material';
import { updateScore, resetBoard, resetGame, setWinningLine, calculateWinner } from './gameSlice';

const GameStatus = () => {
    const dispatch = useDispatch();
    const xIsNext = useSelector((state) => state.game.xIsNext);
    const xWins = useSelector((state) => state.game.xWins);
    const oWins = useSelector((state) => state.game.oWins);
    const board = useSelector((state) => state.game.board);
    const { winner, line } = calculateWinner(board);

    useEffect(() => {
        if (winner) {
            dispatch(setWinningLine(line));
            dispatch(updateScore({ winner }));
        }
    }, [winner, dispatch]);

    const handleResetBoard = () => {
        dispatch(resetBoard());
    };

    const handleResetGame = () => {
        dispatch(resetGame());
    };

    let status;
    if (winner) {
        status = `Winner: ${winner}`;
    } else {
        status = `Next player: ${xIsNext ? 'X' : 'O'}`;
    }

    return (
        <Box textAlign="center" mb={2}>
            <Typography variant="h6" gutterBottom>
                {status}
            </Typography>
            <Typography variant="h6" gutterBottom>
                X Wins: {xWins} | O Wins: {oWins}
            </Typography>
            <Button variant="contained" color="primary" onClick={handleResetBoard}>
                Reset Board
            </Button>
            <Button variant="contained" color="secondary" onClick={handleResetGame} style={{ marginLeft: '10px' }}>
                Reset Game
            </Button>
        </Box>
    );
};

export default GameStatus;
