import {Grid} from '@mui/material';
import Cell from './Cell.jsx';

const Board = ({board, onClick, winningLine}) => {
    return (
        <Grid container spacing={2}>
            {board.map((value, index) => (
                <Grid item xs={4} key={index}>
                    <Cell
                        value={value}
                        onClick={() => onClick(index)}
                        highlight={winningLine && winningLine.includes(index)}
                    />
                </Grid>
            ))}
        </Grid>
    );
};

export default Board;
