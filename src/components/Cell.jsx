import {Paper} from '@mui/material';

const Cell = ({value, onClick, highlight}) => {
    return (
        <Paper
            onClick={onClick}
            style={{
                height: 100,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                fontSize: '4rem',
                cursor: 'pointer',
                backgroundColor: highlight ? 'lightgreen' : 'white'
            }}
        >
            {value}
        </Paper>
    );
};

export default Cell;
