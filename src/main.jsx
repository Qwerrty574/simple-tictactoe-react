import React from 'react';
import {Provider} from 'react-redux';
import App from './App';
import {store} from "./app/store.js";
import {CssBaseline, ThemeProvider} from "@mui/material";
import theme from './theme';
import {createRoot} from "react-dom/client";

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
    <React.StrictMode>
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <CssBaseline/>
                <App/>
            </ThemeProvider>
        </Provider>
    </React.StrictMode>,
)
