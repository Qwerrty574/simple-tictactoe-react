import {Container} from "@mui/material";
import {GameStatus, GameBoard} from "./features/game";

const App = () => (
    <Container maxWidth="sm">
        <GameStatus/>
        <GameBoard/>
    </Container>
);

export default App;
